# Instruções #

Aqui trago algumas informações referente ao App e algumas instruções.
[Link do projeto antigo, com todos commits com erro da biblioteca do maps.](https://bitbucket.org/leocassioo/hinovamobileold)


### Informativos e guias ###

* Projeto todo comentado explicando passo a passo. O desenvolvimento do App é totalmente feito em React Native, sendo que toda a lógica do projeto está no diretório raiz do projeto em: índex.android.js (Tela a ser renderizada para Android), índex.ios.js (Tela a ser renderizada para IOS) ambas possuindo apenas o import do arquivo rotas, seguindo o padrão de desenvolvimento Redux.
 Os demais arquivos se encontram organizados na pasta src, possuindo o arquivo com as Rotas.js possuindo todas as rotas das telas do App, e em src/components são encontrados os componentes da aplicação, tais como a tela Inicial, Tela de Compartilhar o App e Listagem das Oficinas. O component Oficinas é responsavel por fazer a requisição HTTP get e trazer os dados.

* Projeto criado e executado usando React native com Xcode 8.2 e Android Studio 2.2.3, mas como havia me solicitado, após a finalização do projeto instalei o xcode 7.2 e Android studio 2.1.3. 
Obtive sucesso em ambos na compilação, como pode ser visto abaixo. O projeto é capaz de compilar em Android e IOS e várias versões diferentes.

### Xcode 8.2.1 ###
![xcode 8.2.1.png](https://bitbucket.org/repo/k8Ey4n/images/2592747414-xcode%208.2.1.png)
### Xcode 7.2 ###
![xcode 7.2.png](https://bitbucket.org/repo/k8Ey4n/images/4212318044-xcode%207.2.png)
![2xcode 7.2.png](https://bitbucket.org/repo/k8Ey4n/images/397212344-2xcode%207.2.png)
![3xcode 7.2.png](https://bitbucket.org/repo/k8Ey4n/images/1196091199-3xcode%207.2.png)
![4xcode 7.2.png](https://bitbucket.org/repo/k8Ey4n/images/872969728-4xcode%207.2.png)
### Android Studio 2.2.3 ###
![Android Studio 2.2.3.png](https://bitbucket.org/repo/k8Ey4n/images/3799979024-Android%20Studio%202.2.3.png)
### Android Studio 2.1.3 ###
Caso na compilação der algum erro referente a versões faça o seguinte:
###### 1 - No Android Studio vá em Settings -> Build, Execution, Deployment option -> Instant Run ######
###### 2 - Desmarque todas as 4 opções ######
###### 3 - De um Clean Project e o compile novamente ######
![android studio 2.1.3.png](https://bitbucket.org/repo/k8Ey4n/images/3027987627-android%20studio%202.1.3.png)

### Instalação do React native  ###

Será necessario a instalação do react native a primeiro momento, pois não fora geradas releases do App. [Basta seguir as instruções](https://facebook.github.io/react-native/docs/getting-started.html), muitos simples e fáceis de fazer a instalação. 

Apos a instalação do mesmo e baixar o projeto, navegue ate a pasta baixada entre no diretório e de o seguinte comando para compilar e rodar no simulador IOS (Necessário o Xcode instalado) 
```
#!javascript

react-native run-ios
```
Caso esteja no linux ou windows no terminal no diretório do projeto, será necessário ter aberto o Android studio na pasta do projeto/Android com o emulador aberto  de o seguinte comando

```
#!javascript

react-native start
```
E depois abra outro terminal e de o seguinte comando
```
#!javascript

react-native run-android
```

### Compilar direto pelo Xcode e Android Studio ###

#### Xcode ####
Vá em abrir um projeto existente, navegue até a pasta do projeto e vá na pasta IOS, la terá um projeto com a extensão do Xcode.

#### Android Studio ####
Vá em abrir um projeto existente, navegue ate a pasta do projeto e vá no diretório android e de ok


### Design ###

No diretório [docs](https://bitbucket.org/leocassioo/hinovamobileapp/src/dee2062ae1dd67e29866697acf5b18ae92859f7f/docs/?at=master) voce encontrara o design das telas do App no formato .AI (Illustrator)

### Dúvidas? ###

Sobre o desempenho do React Native confira mais [neste artigo.](https://medium.com/the-react-native-log/comparing-the-performance-between-native-ios-swift-and-react-native-7b5490d363e2#.hcht15j5e)

###### Estou a disposição ######
* Cell: 31 98355-8266 / 99890-8266
* Email: Leonardo-cassio10@hotmail.com
* Linkedin: https://www.linkedin.com/in/leocassioo/