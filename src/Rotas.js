// imports do react e react native
import React from 'react';
import { StyleSheet } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';

// import dos nossos proprios components
import Inicial from './components/Inicial';
import ListaOficinas from './components/ListaOficinas';
import Compartilhar from './components/Compartilhar';

//criando nossas rotas do app, utilizando o router flux
const Rotas = () => (
	<Router sceneStyle={{ paddingTop: 65 }}>

		<Scene 
			key='inicial' 
			titleStyle={styles.txtNavBar}
			navigationBarStyle={{ backgroundColor: '#1d3a6b' }} 
			component={Inicial} //invocando tela inicial
			initil 
			title={'Hinova Mobile App'} 
		/>

		<Scene 
			key='ListaOficinas' 
			titleStyle={styles.txtNavBar}
			navigationBarStyle={{ backgroundColor: '#1d3a6b' }} 
			component={ListaOficinas} 
			title={'Lista de Oficinas'} 
		/>
		<Scene 
			key='Compartilhar' 
			titleStyle={styles.txtNavBar}
			navigationBarStyle={{ backgroundColor: '#1d3a6b' }} 
			component={Compartilhar}  //invocando tela de compartilhar
			title={'Compartilhar App'} 
		/>

	</Router>

	);

// estilos do app
const styles = StyleSheet.create({
	txtNavBar: {
		color: '#fff',
		fontSize: 20,
		fontWeight: 'bold',
		alignSelf: 'center'
	}
});
// export do componente
export default Rotas;
