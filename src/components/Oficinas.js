//import do react
import React, { Component } from 'react';
//import dos componentes do React Native
import { 
	Text,
	Image,
	StyleSheet,
	View,
	MapView,
	TouchableOpacity
} from 'react-native';

//import componente de avaliacao
import  Rating from 'react-native-easy-rating';
//import do componente responsavel pela pessoa clicar no 
//telefone, email dentre outros para abrir 
//seus respectvos apps externos
import Communications from 'react-native-communications';

//export default para a classe poder ser importada em outros componentes
export default class Oficinas extends Component {

//aqui ocorre a renderizacao do App
  render() {
	//recuperando a hash da api que nao veio em formato png, 
	// e fazendo a transformacao no component Image 
	// da hash 64 (data:image/gif;base64,${img}) para o formato de imagem
	const img = this.props.oficina.Foto;

	//defininco o local, sendo passado via props, pegando os dados da API
	//de latitude e longitude
	const local = {
		latitude: this.props.oficina.Latitude,
		longitude: this.props.oficina.Longitude,
		//zoom no mapa(aproximacao)
		latitudeDelta: 0.004757,
		longitudeDelta: 0.006866
	};
	//definindo a marcaco no map em latitude e longitude, e o titulo ao clicar no pin de marcacao
	const markers = [{
    latitude: this.props.oficina.Latitude,
    longitude: this.props.oficina.Longitude,
    title: this.props.oficina.Nome,
    subtitle: this.props.oficina.DescricaoCurta
	}];

	//recuperando a descricao para subistituir os  \\n pela quebra de linha do react
	const descReplace = this.props.oficina.Descricao;

	//retorno dos componentes para a criacao da tela, comunicacao 
	//do usuário com o APP
    return (
		<View style={styles.lista}>
			<View style={styles.cabecalho}>
				<View>
					<Image
						style={styles.foto}
						source={{ uri: `data:image/gif;base64,${img}` }}
					/>
				</View>
				<View style={styles.subCabecalho}>
					<Text style={styles.txtTitulo}>{ this.props.oficina.Nome }</Text>
					<Text style={styles.txtDescCurta}>{ this.props.oficina.DescricaoCurta }</Text>

					<View style={styles.telefoneView}>
						<TouchableOpacity onPress={() => Communications.phonecall('{this.props.oficina.Telefone1}', true)}>
							<View style={styles.telefone}>
								<Text style={styles.text}>{ this.props.oficina.Telefone1 }</Text>
							</View>
						</TouchableOpacity>
					</View>
					<View style={styles.telefoneView}>
						<TouchableOpacity onPress={() => Communications.phonecall('{ this.props.oficina.Telefone2 }', true)}>
							<View style={styles.telefone}>
								<Text style={styles.text}>{ this.props.oficina.Telefone2 }</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>

			<View>
				<Text style={styles.descricao}>{ `${descReplace.replace(/\\n/g, "\n")}` }</Text>
			</View>

			<View>
				<TouchableOpacity 
					onPress={() => Communications.email(['this.props.oficina.Email'], 
					null, null, 'Hinova Mobile App Oficina', 'Email enviado atraves do app de oficinas')}>
					<View>
						<Text style={styles.outros}>Contato: { this.props.oficina.Email }</Text>
					</View>
				</TouchableOpacity>
			</View>

			<Text style={styles.outros}>Endereço: { this.props.oficina.Endereco }</Text>

			<View style={styles.avaliacoes}>
				<Rating
					rating={this.props.oficina.AvaliacaoUsuario}
					max={5}
					iconWidth={24}
					iconHeight={24}
					iconSelected={require('../../imgs/select.png')}
					iconUnselected={require('../../imgs/unselect.png')}
					onRate={(rating) => this.setState({ rating: rating })}
				/>
			</View>

			<MapView
				style={{ height: 150 }}
				region={local}
				annotations={markers}
			/>
      </View> 
      );
  }
}

//estilos do APP
const styles = StyleSheet.create({
  lista: {
    backgroundColor: '#fff',
    margin: 10,
    padding: 10,
  },
  cabecalho: {
	flexDirection: 'row'
  },
  subCabecalho: {
	flex: 2
  },
  foto: {
    width: 95,
    height: 95,
    resizeMode: 'contain',
    flex: 1
  },
  txtTitulo: {
    fontSize: 17,
    color: '#000',
    marginTop: 0,
    marginLeft: 15,
	fontWeight: 'bold'
  },
  txtDescCurta: {
    fontSize: 11,
	marginTop: 5,
    marginLeft: 15,
  },
  telefone: {
	fontSize: 15,
	marginTop: 5,
    marginLeft: 0
  },
  telefoneView: {
	marginTop: 5,
    marginLeft: 15
  },
  descricao: {
	fontSize: 15
  },
  outros: {
	marginTop: 7,
	fontSize: 15,
  },
  avaliacoes: {
	alignItems: 'center',
	margin: 10
  },
  container: {
    flex: 1,
    marginLeft: 15
  }
});
