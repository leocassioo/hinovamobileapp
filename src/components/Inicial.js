//imports do react e react native
import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView
} from 'react-native';

// import das actions do router flux, para funcoes de navegacao entre telas
import { Actions } from 'react-native-router-flux';
//import de componentes proprios
import ListaOficinas from './ListaOficinas';
import Oficinas from './Oficinas';

//import da logo, passando seu conteudo para uma variavel
const logo = require('../../imgs/Hinova-Logo.png');

//export default para a classe poder ser importada em outros componentes
export default class Inicial extends Component {
  render() {
    return (
			<ScrollView style={styles.scroll}>
				<View style={styles.principal}>
					<Image source={logo} />

					<View style={styles.btnView}>
						<TouchableOpacity
							onPress={() => { Actions.ListaOficinas(); }}
							style={styles.botao}
						>
							<Text style={styles.txtBtn}>Oficinas</Text>

						</TouchableOpacity>

						<TouchableOpacity
							onPress={() => { Actions.Compartilhar(); }}
							style={styles.botao}
						>
							<Text style={styles.txtBtn}>Compartilhar</Text>

						</TouchableOpacity>
					</View>
				</View>
			</ScrollView>
    );
  }
}

// estilos da pagina do APP
const styles = StyleSheet.create({
	scroll: {
		paddingTop: 70
	},
	principal: {
		alignItems: 'center', 
		justifyContent: 'center'
	},
	botao: {
		borderWidth: 3,
		borderColor: '#346a8f',
		backgroundColor: '#fff',
		paddingVertical: 15,
		paddingHorizontal: 40,
		marginTop: 30,
		borderRadius: 100
	},
	btnView: {
		paddingTop: 70
	},
	txtBtn: {
		color: '#000',
		fontSize: 16,
		fontWeight: 'bold',
		alignSelf: 'center'
	}
});
