// imports do react e react native
import React, { Component } from 'react';
import { ScrollView } from 'react-native';

// impor do axios, responsavem por fazer requisicoes na API
import axios from 'axios';

// import do nosso componente Oficinas
import Oficinas from './Oficinas';

//export default para a classe poder ser importada em outros componentes
export default class ListaOficinas extends Component {

  constructor(props) {
    super(props);
    // definindo o estado inicial da lista de oficinas como um array vazio
    this.state = { listaOficinas: [] };
  }

  componentWillMount() {
    //requisicao HTTP
    axios.get('http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Oficina?codigoAssociacao=601&cpfAssociado=""')
      // nossa promisse de aplicacao com sucesso irar atribuir a nossa listaOficinas
      // o valor restornados da API
      .then(response => { this.setState({ listaOficinas: response.data.ListaOficinas }); })
      // em caso de algum erro, o tratamento de erro
      .catch(() => { console.log('Erro ao recuperar os dados'); });
  }

  render() {
    //renderizando a aplicacao, criando um scrollview e exibindo a lista de oficinas 
    // buscadas na API
    return (
        <ScrollView>
          { this.state.listaOficinas.map(oficina => 
            (<Oficinas key={oficina.Id} oficina={oficina} />)) 
          }
        </ScrollView> 
      );
  }
}
