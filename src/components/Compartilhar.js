//import necessarios para a aplicacao
import React, { Component } from 'react';
import {  
  View, 
  Text, 
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native';

import axios from 'axios';
import TextField from 'react-native-md-textinput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';

//importando as imagens em variaveis, para um melhor proveito no decorrer do codigo
const codigo = require('../../imgs/numeros.png');
const cpf = require('../../imgs/id.png');
const email = require('../../imgs/email.png');
const user = require('../../imgs/user.png');
const telefone = require('../../imgs/telefone.png');
const car = require('../../imgs/car.png');
const data = require('../../imgs/data.png');

//export default para a classe poder ser importada em outros componentes
export default class Compartilhar extends Component {
  //construindo os objetos antes da renderizacao do app
  constructor(props) {
    super(props);

    this.state = { 
      codigoAssociacao: this.props.codigoAssociacao,
      dataCriacao: this.props.dataCriacao,
      cpfAssociado: this.props.cpfAssociado,
      emailAssociado: this.props.emailAssociado,
      nomeAssociado: this.props.nomeAssociado,
      telefoneAssociado: this.props.telefoneAssociado,
      placaVeiculoAssociado: this.props.placaVeiculoAssociado,
      nomeAmigo: this.props.nomeAmigo,
      telefoneAmigo: this.props.telefoneAmigo,
      emailAmigo: this.props.emailAmigo
    };
  }
  //funcao responsavel por enviar os dados para a API
  enviarIndicacao() {
    // Enviando os dados via post
    axios.post('http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Indicacao', {

      Indicacao: {
        "CodigoAssociacao": this.state.codigoAssociacao,
        "DataCriacao": this.state.dataCriacao,
        "CpfAssociado": this.state.cpfAssociado,
        "EmailAssociado": this.state.emailAssociado,
        "NomeAssociado": this.state.nomeAssociado,
        "TelefoneAssociado": this.state.telefoneAssociado,
        "PlacaVeiculoAssociado": this.state.placaVeiculoAssociado,
        "NomeAmigo": this.state.nomeAmigo,
        "TelefoneAmigo": this.state.telefoneAmigo,
        "EmailAmigo": this.state.emailAmigo

      },
      "Remetente":"romulo.marques@hinovamobile.com.br",
      "Copias": []


      })
      //promisse (promessa) caso nossa funcao de certo
      .then((response) => {
        //alert infomando ao usuario que ele nao apreencheu todos os campos 
        if (response.data.Sucesso === null) {
          Alert.alert(
            'Erro',
            'Por favor preencha todos os campos correramente!',
            [ 
              { text: 'ok' }
            ]
            );
        } else { 
          //em caso de sucesso, apresentara um alert infomando que foi bem sucedido
          //e o usuario sera redirecionado para a tela inicial do APP
            Alert.alert(
            'Sucesso',
            'Indicação enviada com sucesso!',
            [ 
              { text: 'ok', onPress: () => Actions.pop() }
            ]
            );
         }
      })
      //tratamento caso ocorra algum erro, tais como sem internet ou servidor indisponivel
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    //criacao dos componentes da tela
    return (

      <KeyboardAwareScrollView>
        <View style={styles.viewTitulo}>
          <Text style={styles.titulo}>Compartilhe com seus amigos!</Text>
        </View>

        <View style={styles.viewEntradatxt}>
          <View style={styles.viewTitulo}>
            <Text style={styles.subtitulo}>Dados do Associado</Text>
          </View>
          
          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={codigo} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField
                label={'Código da Associação'}
                highlightColor={'#4DBCDA'}
                keyboardType={'numeric'}
                value={this.state.codigoAssociacao}
                onChangeText={(text) => this.setState({ codigoAssociacao: text })}
                
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={user} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Nome'} 
                highlightColor={'#4DBCDA'}
                value={this.state.nomeAssociado}
                onChangeText={(text) => this.setState({ nomeAssociado: text })}
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={cpf} />
            </View>
            <View style={{ flex: 8 }}>

              <TextField 
                label={'CPF'} 
                highlightColor={'#4DBCDA'} 
                keyboardType={'numeric'}
                value={this.state.cpfAssociado}
                onChangeText={(text) => this.setState({ cpfAssociado: text })}
              />
            </View>
          </View>
          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={data} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Data Criação'} 
                highlightColor={'#4DBCDA'} 
                keyboardType={'numeric'}
                value={this.state.dataCriacao}
                onChangeText={(text) => this.setState({ dataCriacao: text })}
              />
            </View>
          </View>
            

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={email} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Email'} 
                highlightColor={'#4DBCDA'}
                keyboardType={'email-address'}
                value={this.state.emailAssociado}
                onChangeText={(text) => this.setState({ emailAssociado: text })}
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={telefone} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Telefone'} 
                highlightColor={'#4DBCDA'} 
                keyboardType={'phone-pad'}
                value={this.state.telefoneAssociado}
                onChangeText={(text) => this.setState({ telefoneAssociado: text })}
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={car} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField
                label={'Placa do Veículo'} 
                highlightColor={'#4DBCDA'} 
                value={this.state.placaVeiculoAssociado}
                onChangeText={(text) => this.setState({ placaVeiculoAssociado: text })}
              />
            </View>
          </View>

          <View style={styles.viewTitulo}>
            <Text style={styles.subtitulo}>Indicar para...</Text>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={user} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Nome '} 
                highlightColor={'#4DBCDA'} 
                value={this.state.nomeAmigo}
                onChangeText={(text) => this.setState({ nomeAmigo: text })}
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={telefone} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Telefone'} 
                highlightColor={'#4DBCDA'} 
                keyboardType={'phone-pad'}
                value={this.state.telefoneAmigo}
                onChangeText={(text) => this.setState({ telefoneAmigo: text })}
              />
            </View>
          </View>

          <View style={styles.viewPai}>
            <View style={styles.viewFilho}>
              <Image style={styles.icons} source={email} />
            </View>
            <View style={{ flex: 8 }}>
              <TextField 
                label={'Email'} 
                highlightColor={'#4DBCDA'} 
                keyboardType={'email-address'}
                value={this.state.emailAmigo}
                onChangeText={(text) => this.setState({ emailAmigo: text })}
              />
            </View>
          </View>
        </View>
        <View style={styles.btnView}>
          <View>
            <TouchableOpacity
              onPress={() => {
                this.enviarIndicacao(); 
              }}
              style={styles.botao}
            >
              <Text style={styles.txtBtn}>Enviar</Text>

            </TouchableOpacity>
          </View>
          </View>

      </KeyboardAwareScrollView>
      );
  }
}
//estilizacao da pagina do app
const styles = StyleSheet.create({
    titulo: {
      margin: 10,
      fontSize: 20,
      fontWeight: 'bold'
    },
    subtitulo: {
      fontSize: 16,
      fontWeight: 'bold'
    },
    viewTitulo: {
      alignItems: 'center',
      marginTop: 7
    },
    viewEntradatxt: {
      marginLeft: 10,
      marginRight: 10
    },
    txtInput: {
      height: 40, 
      borderColor: 'gray', 
      borderWidth: 1
    },
    botao: {
      borderWidth: 3,
      borderColor: '#346a8f',
      backgroundColor: '#fff',
      paddingVertical: 15,
      paddingHorizontal: 60,
      marginTop: 30,
      borderRadius: 100
  },
  viewPai: {
    flexDirection: 'row'
  },
  viewFilho: {
    flex: 1,
    marginTop: 35
  },
  icons: {
    width: 25,
    height: 25
  },
  data: {
    fontSize: 17,
    marginTop: 35
  },
  btnView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 50
  },
  txtBtn: {
    color: '#000',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center'
  }
});
