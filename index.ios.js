//iport do react
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

// import do nosso compoente Rotas
import Rotas from './src/Rotas';

export default class HinovaMobileApp extends Component {
  render() {
    return (
      <Rotas />
    );
  }
}

// componente root do app para o carregamente do bundle da aplicacao
AppRegistry.registerComponent('HinovaMobileApp', () => HinovaMobileApp);
